package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Customer")
public class Customer {	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customerid")
	private int customerid;
@Column(name = "firstname")
	private String firstname;
	@Column(name = "lastname")
	private String lastname;
	@Column(name = "tel")
	private String tel;
	@Column(name = "birthday")
	private String birthday;
	@Column(name = "walletid")
	private int walletid;
	@Column(name = "username")
	private String username;
	public Customer(int customerid, String firstname, String lastname, String tel, String birthday, int walletid,
			String username) {
		super();
		this.customerid = customerid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.tel = tel;
		this.birthday = birthday;
		this.walletid = walletid;
		this.username = username;
	}
	public Customer(String firstname, String lastname, String tel, String birthday, int walletid, String username) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.tel = tel;
		this.birthday = birthday;
		this.walletid = walletid;
		this.username = username;
	}
	public Customer(String firstname, String lastname, String tel, String birthday, String username) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.tel = tel;
		this.birthday = birthday;
		this.username = username;
	}
	public Customer() {
		super();
	}
	public int getCustomerid() {
		return customerid;
	}
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getWalletid() {
		return walletid;
	}
	public void setWalletid(int walletid) {
		this.walletid = walletid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	

}
