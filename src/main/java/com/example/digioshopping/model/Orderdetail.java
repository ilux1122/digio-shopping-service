package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Orderdetail")
public class Orderdetail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "orderdetailid")
	private int orderdetailid;
	@Column(name = "productid")
	private int productid ;
	@Column(name = "productname")
	private String productname ;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "price")
	private Double price;
	@Column(name = "orderid")
	private String orderid;
	
	public Orderdetail(int orderdetailid, int productid, String productname, int quantity, Double price,
			String orderid) {
		super();
		this.orderdetailid = orderdetailid;
		this.productid = productid;
		this.productname = productname;
		this.quantity = quantity;
		this.price = price;
		this.orderid = orderid;
	}
	public Orderdetail(int productid,String productname, int quantity, Double price, String orderid) {
		super();
		this.productid = productid;
		this.productname = productname;
		this.quantity = quantity;
		this.price = price;
		this.orderid = orderid;
	}
	
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public void setOrderdetailid(int orderdetailid) {
		this.orderdetailid = orderdetailid;
	}
	public Orderdetail() {
		super();
	}
	public int getOrderdetailid() {
		return orderdetailid;
	}
	public void setOrderDetailid(int orderdetailid) {
		this.orderdetailid = orderdetailid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
}
