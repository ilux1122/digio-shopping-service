package com.example.digioshopping.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Product")
public class Product {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "productid")
	private int productid;
	@Column(name = "productname")
	private String productname;
	@Column(name = "productprice")
	private Double productprice;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "productimg")
	private String productimg;
	@Column(name = "description")
	private String description;
	@Column(name = "merchantid")
	private int merchantid;
	@Column(name = "producttypeid")
	private int producttypeid;

	/*
	 * @JoinColumn(name = "merchantid")
	 * 
	 * @ManyToOne(cascade = CascadeType.ALL) private Merchant merchant;
	 * 
	 * @JoinColumn(name = "producttypeid")
	 * 
	 * @ManyToOne(cascade = CascadeType.ALL) private Producttype producttype;
	 */
	
	public Product() {
		super();
	}
	public Product(int productid, String productname, Double productprice, int quantity, String productimg,
			String description, int merchantid, int producttypeid) {
		super();
		this.productid = productid;
		this.productname = productname;
		this.productprice = productprice;
		this.quantity = quantity;
		this.productimg = productimg;
		this.description = description;
		this.merchantid = merchantid;
		this.producttypeid = producttypeid;
	}
	
	public Product(String productname, Double productprice, int quantity, String productimg, String description,
			int merchantid, int producttypeid) {
		super();
		this.productname = productname;
		this.productprice = productprice;
		this.quantity = quantity;
		this.productimg = productimg;
		this.description = description;
		this.merchantid = merchantid;
		this.producttypeid = producttypeid;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public Double getProductprice() {
		return productprice;
	}
	public void setProductprice(Double productprice) {
		this.productprice = productprice;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getProductimg() {
		return productimg;
	}
	public void setProductimg(String productimg) {
		this.productimg = productimg;
	}
	public int getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(int merchantid) {
		this.merchantid = merchantid;
	}
	public int getProducttypeid() {
		return producttypeid;
	}
	public void setProducttypeid(int producttypeid) {
		this.producttypeid = producttypeid;
	}
	
}
