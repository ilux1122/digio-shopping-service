package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Producttype")
public class Producttype {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "producttypeid")
	private int producttypeid;
	@Column(name = "typename")
	private String typename;
	public Producttype(int producttypeid, String typename) {
		super();
		this.producttypeid = producttypeid;
		this.typename = typename;
	}
	public Producttype() {
		super();
	}
	public int getProducttypeid() {
		return producttypeid;
	}
	public void setProducttypeid(int producttypeid) {
		this.producttypeid = producttypeid;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	
	

}
