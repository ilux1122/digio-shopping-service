package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * @author IceNMind
 *
 */
@Entity
@Table(name = "Transaction")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "transactionid")
	private int transactionid;
	@Column(name = "customerid")
	private int customerid;
	@Column(name = "transactiontype")
	private String transactiontype;
	@Column(name = "amount")
	private double amount;
	@Column(name = "transactiontime")
	private String transactiontime;
	@Column(name = "orderid")
	private String orderid;
	
	public Transaction(int customerid, String transactiontype, double amount, String transactiontime, String orderid) {
		super();
		this.customerid = customerid;
		this.transactiontype = transactiontype;
		this.amount = amount;
		this.transactiontime = transactiontime;
		this.orderid = orderid;
	}
	public Transaction(int transactionid, int customerid, String transactiontype, double amount, String transactiontime,
			String orderid) {
		super();
		this.transactionid = transactionid;
		this.customerid = customerid;
		this.transactiontype = transactiontype;
		this.amount = amount;
		this.transactiontime = transactiontime;
		this.orderid = orderid;
	}
	public Transaction() {
		super();
	}
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public int getTransactionid() {
		return transactionid;
	}
	public void setTransactionid(int transactionid) {
		this.transactionid = transactionid;
	}
	public int getCustomerid() {
		return customerid;
	}
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}
	public String getTransactiontype() {
		return transactiontype;
	}
	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTransactiontime() {
		return transactiontime;
	}
	public void setTransactiontime(String transactiontime) {
		this.transactiontime = transactiontime;
	}
	

}
