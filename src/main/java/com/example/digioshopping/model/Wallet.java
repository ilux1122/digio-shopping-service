package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Wallets")
public class Wallet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "walletid")
	private int walletid;
	@Column(name = "money")
	private double money;
	@Column(name = "secretkey")
	private String secretkey;
	public Wallet(int walletid, double money, String secretkey) {
		super();
		this.walletid = walletid;
		this.money = money;
		this.secretkey = secretkey;
	}
	public Wallet(double money, String secretkey) {
		super();
		this.money = money;
		this.secretkey = secretkey;
	}

	public Wallet(double money) {
		super();
		this.money = money;
	}
	public Wallet() {
		super();
	}
	public int getWalletid() {
		return walletid;
	}
	public void setWalletid(int walletid) {
		this.walletid = walletid;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getSecretkey() {
		return secretkey;
	}
	public void setSecretkey(String secretkey) {
		this.secretkey = secretkey;
	}
	
	

}
