package com.example.digioshopping.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Merchant")
public class Merchant {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "merchantid")
	private int merchantid;
	@Column(name = "merchantname")
	private String merchantname;
	@Column(name = "username")
	private String username;
	@Column(name = "walletid")
	private int walletid;

	

	public Merchant(String merchantname, String username, int walletid) {
		super();
		this.merchantname = merchantname;
		this.username = username;
		this.walletid = walletid;
	}

	public Merchant(int merchantid, String merchantname, String username, int walletid) {
		super();
		this.merchantid = merchantid;
		this.merchantname = merchantname;
		this.username = username;
		this.walletid = walletid;
	}

	public Merchant(String merchantname, String username) {
		super();
		this.merchantname = merchantname;
		this.username = username;
	}

	public Merchant(int merchantid, String merchantname, String username) {
		super();
		this.merchantid = merchantid;
		this.merchantname = merchantname;
		this.username = username;
	}

	public Merchant() {
		super();
	}

	public int getWalletid() {
		return walletid;
	}

	public void setWalletid(int walletid) {
		this.walletid = walletid;
	}

	public int getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(int merchantid) {
		this.merchantid = merchantid;
	}

	public String getMerchantname() {
		return merchantname;
	}

	public void setMerchantname(String merchantname) {
		this.merchantname = merchantname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	
	
	

}
