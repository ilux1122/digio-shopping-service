package com.example.digioshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Orders")
public class Orders {
	@Id
	@Column(name = "orderid")
	private String orderid;

	@Column(name = "customername")
	private String customername;
	@Column(name = "transactiontime")
	private String transactiontime;
	@Column(name = "totalprice")
	private Double totalprice;
	@Column(name = "merchantid")
	private int merchantid;
	@Column(name = "paymentstatus")
	private String paymentstatus;
	
	public Orders(String customername, String transactiontime, Double totalprice, int merchantid,
			String paymentstatus) {
		super();
		this.customername = customername;
		this.transactiontime = transactiontime;
		this.totalprice = totalprice;
		this.merchantid = merchantid;
		this.paymentstatus = paymentstatus;
	}
	public Orders(String orderid, String customername, String transactiontime, Double totalprice, int merchantid,
			String paymentstatus) {
		super();
		this.orderid = orderid;
		this.customername = customername;
		this.transactiontime = transactiontime;
		this.totalprice = totalprice;
		this.merchantid = merchantid;
		this.paymentstatus = paymentstatus;
	}
	public Orders() {
		super();
	}
	public String getPaymentstatus() {
		return paymentstatus;
	}
	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getTransactiontime() {
		return transactiontime;
	}
	public void setTransactiontime(String transactiontime) {
		this.transactiontime = transactiontime;
	}
	public Double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(Double totalprice) {
		this.totalprice = totalprice;
	}
	public int getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(int merchantid) {
		this.merchantid = merchantid;
	}
}
