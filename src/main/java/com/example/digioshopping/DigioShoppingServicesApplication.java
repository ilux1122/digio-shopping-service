package com.example.digioshopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigioShoppingServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigioShoppingServicesApplication.class, args);
	}

}
