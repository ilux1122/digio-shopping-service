package com.example.digioshopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.Orders;
import com.example.digioshopping.model.Wallet;
import com.example.digioshopping.repository.MerchantRepository;
import com.example.digioshopping.repository.WalletRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/merchant")
public class MerchantController {
	Response re = new Response();
	Validate vd = new Validate();

	@Autowired
	MerchantRepository merchantRespository;

	@Autowired
	WalletRepository walletRepository;
	
	@GetMapping
	public List<Merchant> index() {
		return merchantRespository.findAll();
	}

	@GetMapping("/{username}")
	public Merchant show(@PathVariable String username) {
		return merchantRespository.findByUsername(username);
	}

	@PostMapping
	public Response createMerchant(@RequestBody Merchant mc) {
		int max = 1;
		List<Wallet> wl = null;
		try {
			wl = walletRepository.findAll();
			max = wl.get(wl.size()-1).getWalletid();
		} catch (Exception e) {
			System.out.println("error ");
			max = 1;
		}
		try {
		re = vd.SetMerchant(mc);
		if (re.getCode() == "000") {
			Merchant m = merchantRespository.save(new Merchant(mc.getMerchantname(), mc.getUsername(),max));
			return vd.SetMerchantsuccess(m);
		} else {
			return re;
		}}catch(Exception e) {
			return vd.SetMerchantsuccess(null);
		}
	}

	@PutMapping("/{id}")
	public Response updateMerchant(@PathVariable String id, @RequestBody Merchant mc) {
		try {
		re = vd.UpdateMerchantfromID(id,mc);
		if (re.getCode() == "000") {
			Merchant m = merchantRespository
					.save(new Merchant(Integer.parseInt(id), mc.getMerchantname(), mc.getUsername()));
			return vd.UpdateMerchantfromIDsuccess(m);
		} else {
			return re;
		}}catch(Exception e) {
			return vd.UpdateMerchantfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteMerchant(@PathVariable String id) {
		try {
		re = vd.DeleteMerchant(id);
		if (re.getCode() == "000") {
			try {
				merchantRespository.deleteById(Integer.parseInt(id));
				return vd.DeleteMerchantsuccess(true);
			} catch (Exception e) {
				return vd.DeleteMerchantsuccess(false);
			}
		}
		return vd.DeleteMerchantsuccess(false);
	}catch(Exception e) {
		return vd.DeleteMerchantsuccess(false);
	}
	}

}
