package com.example.digioshopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Customer;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.User;
import com.example.digioshopping.model.Wallet;
import com.example.digioshopping.repository.CustomerRepository;
import com.example.digioshopping.repository.MerchantRepository;
import com.example.digioshopping.repository.UserRepository;
import com.example.digioshopping.repository.WalletRepository;

@CrossOrigin
@RestController
@RequestMapping("/customer")
public class CustomerController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	WalletRepository walletRepository;
	@GetMapping
	public List<Customer> index() {
		return customerRepository.findAll();
	}
	

	@GetMapping("/max")
	public int maxid() {
		int max = 0;
		List<Customer> wl = null;
		try {
			wl = customerRepository.findAll();
			max = wl.get(wl.size()-1).getCustomerid();
		} catch (Exception e) {
			System.out.println("error ");
			max = 0;
		}
		return max;
	}
	@GetMapping("/username/{user}")
	public Customer showusername(@PathVariable String user) {
		return customerRepository.findByUsername(user);
	}
	@GetMapping("/tel/{tel}")
	public Customer showtel(@PathVariable String tel) {
		return customerRepository.findByTel(tel);
	}
	@GetMapping("/id/{id}")
	public List<Customer> showid(@PathVariable String id) {
		return customerRepository.findById(Integer.parseInt(id));
	}
	@PostMapping
	public Response createCustomer(@RequestBody Customer user) {
		int max = 1;
		List<Wallet> wl = null;
		try {
			wl = walletRepository.findAll();
			max = wl.get(wl.size()-1).getWalletid();
		} catch (Exception e) {
			System.out.println("error ");
			max = 1;
		}
		try {user.setWalletid(max);
			re = vd.SetCustomer(user);
			if (re.getCode() == "000") {
				if (customerRepository.findByUsername(user.getUsername()) == null) {

					Customer u = customerRepository.save(new Customer(user.getFirstname(),user.getLastname(),user.getTel(),user.getBirthday(),max,user.getUsername()));
					
					return vd.SetCustomersuccess(u);
				}
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetCustomersuccess(null);
		}
		return vd.SetCustomersuccess(null);
	}

	@PutMapping("/{id}")
	public Response updateUser(@PathVariable String id, @RequestBody Customer user) {
		try {
			re = vd.UpdateCustomerfromID(id, user);
			if (re.getCode() == "000") {
				Customer u = customerRepository.save(new Customer(user.getFirstname(),user.getLastname(),user.getTel(),user.getBirthday(),user.getWalletid(),user.getUsername()));
				return vd.UpdateCustomerfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateCustomerfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteCustomer(@PathVariable String id) {
		try {
			re = vd.DeleteCustomer(id);
			if (re.getCode() == "000") {
				try {
					customerRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteCustomersuccess(true);
				} catch (Exception e) {
					return vd.DeleteCustomersuccess(false);
				}
			}
			return vd.DeleteCustomersuccess(false);
		} catch (Exception e) {
			return vd.DeleteCustomersuccess(false);
		}
	}

}
