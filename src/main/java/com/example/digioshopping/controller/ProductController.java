package com.example.digioshopping.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Orderdetail;
import com.example.digioshopping.model.Product;
import com.example.digioshopping.repository.ProductRepository;

@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {

	Response re = new Response();
	Validate vd = new Validate();

	@Autowired
	ProductRepository productRespository;

	@GetMapping
	public List<Product> index() {
		return productRespository.findAll();
	}

	@GetMapping("/{id}")
	public Product show(@PathVariable String id) {
		int ptid = Integer.parseInt(id);
		return productRespository.findByproductid(ptid);
	}

	@GetMapping("/merchant/{id}")
	public List<Product> showproductbymerchant(@PathVariable String id) {
		int ptid = Integer.parseInt(id);
		return productRespository.findBymerchantid(ptid);
	}

	@PostMapping
	public Response createProduct(@RequestBody Product pd) {
		try {
			re = vd.SetProduct(pd);
			if (re.getCode() == "000") {
				Product p = productRespository.save(pd);
				return vd.SetProductsuccess(p);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetProductsuccess(null);
		}
	}

	@PutMapping("/{id}")
	public Response updateProduct(@PathVariable String id, @RequestBody Product pd) {
		try {
			re = vd.UpdateProductfromID(id, pd);
			if (re.getCode() == "000") {
				Product p = productRespository
						.save(new Product(Integer.parseInt(id), pd.getProductname(), pd.getProductprice(),
								pd.getQuantity(), pd.getProductimg(),pd.getDescription(), pd.getMerchantid(), pd.getProducttypeid()));
				return vd.UpdateProductfromIDsuccess(p);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateProductfromIDsuccess(null);
		}
	}
	
	@PutMapping("/updateqty")
	public Response updateListProduct(@RequestBody List<Orderdetail> pd) {

		System.out.println("updateqty");
		System.out.println("oid"+pd.get(0).getOrderid());
		System.out.println("pid"+pd.get(0).getProductid());
		System.out.println("pname"+pd.get(0).getProductname());
		System.out.println("qty"+pd.get(0).getQuantity());
		System.out.println("p"+pd.get(0).getPrice());
		try {
			re = vd.UpdateOrderDetailList(pd);
			System.out.println("re"+re.getCode()+" "+re.getMessage());
			if (re.getCode() == "000") {
				for(Orderdetail o : pd) {
					Product p = productRespository.findByproductid(o.getProductid());
					int qty = p.getQuantity() - o.getQuantity();
				productRespository.save(new Product(o.getProductid(), p.getProductname(), p.getProductprice(),
						qty, p.getProductimg(),p.getDescription(), p.getMerchantid(), p.getProducttypeid()));
				}
				return re;
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateProductfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteProduct(@PathVariable String id) {
		try {
			re = vd.DeleteProduct(id);
			if (re.getCode() == "000") {
				try {
					productRespository.deleteById(Integer.parseInt(id));
					return vd.DeleteProductsuccess(true);
				} catch (Exception e) {
					return vd.DeleteProductsuccess(false);
				}
			}
			return vd.DeleteProductsuccess(false);
		} catch (Exception e) {
			return vd.DeleteProductsuccess(false);
		}
	}
}
