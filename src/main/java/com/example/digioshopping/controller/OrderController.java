package com.example.digioshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Orders;
import com.example.digioshopping.repository.OrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/orders")
public class OrderController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	OrderRepository orderRepository;

	@GetMapping
	public List<Orders> index() {
		return orderRepository.findAll();
	}
	

	@GetMapping("/getfrommerchant/{id}")
	public List<Orders> show(@PathVariable String id) {
		return orderRepository.findByMerchantid(Integer.parseInt(id));
	}

	@PostMapping
	public Response createMerchant(@RequestBody Orders or) {
		try {
			re = vd.SetOrder(or);
			if (re.getCode() == "000") {
				Orders o = orderRepository.save(or);
				return vd.SetOrdersuccess(o);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetOrdersuccess(null);
		}
	}

	@PutMapping("/{id}")
	public Response updateMerchant(@PathVariable String id, @RequestBody Orders or) {
		try {
			re = vd.UpdateOrderfromID(id, or);
			if (re.getCode() == "000") {
				Orders o = orderRepository
						.save(new Orders(id, or.getTransactiontime(), or.getTotalprice(), or.getMerchantid(), or.getPaymentstatus()));
				return vd.UpdateOrderfromIDsuccess(o);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateOrderfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteMerchant(@PathVariable String id) {
		try {
			re = vd.DeleteOrder(id);
			if (re.getCode() == "000") {
				try {
					orderRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteOrdersuccess(true);
				} catch (Exception e) {
					return vd.DeleteOrdersuccess(false);
				}
			}
			return vd.DeleteOrdersuccess(false);
		} catch (Exception e) {
			return vd.DeleteOrdersuccess(false);
		}
	}
}
