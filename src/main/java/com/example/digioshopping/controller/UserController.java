package com.example.digioshopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.User;
import com.example.digioshopping.repository.MerchantRepository;
import com.example.digioshopping.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	UserRepository userRepository;
	MerchantRepository merchantRespository;

	@GetMapping
	public List<User> index() {
		return userRepository.findAll();
	}
	@GetMapping("/start")
	public String start() {
		return "true";
	}
	@GetMapping("/username/{user}")
	public User showusername(@PathVariable String user) {
		return userRepository.findByUsername(user);
	}

	@PostMapping
	public Response createUser(@RequestBody User user) {
		System.out.println(user.getEmail()+" "+user.getEmail()+" "+user.getPassword()+" "+user.getUsertype());
		try {
			re = vd.SetUser(user);
			if (re.getCode() == "000") {
				if (userRepository.findByUsername(user.getUsername()) == null) {

					User u = userRepository.save(new User(user.getUsername(), user.getPassword(),user.getEmail(),user.getUsertype()));
					return vd.SetUsersuccess(u);
				}
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetUsersuccess(null);
		}
		return vd.SetUsersuccess(null);
	}

	@PutMapping("/{username}")
	public Response updateUser(@PathVariable String id, @RequestBody User user) {
		try {
			re = vd.UpdateUserfromID(id, user);
			if (re.getCode() == "000") {
				User u = userRepository.save(new User(user.getUsername(), user.getPassword(),user.getEmail(),user.getUsertype()));
				return vd.UpdateUserfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateUserfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{username}")
	public Response deleteUser(@PathVariable String id) {
		try {
			re = vd.DeleteUser(id);
			if (re.getCode() == "000") {
				try {
					userRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteUsersuccess(true);
				} catch (Exception e) {
					return vd.DeleteUsersuccess(false);
				}
			}
			return vd.DeleteUsersuccess(false);
		} catch (Exception e) {
			return vd.DeleteUsersuccess(false);
		}
	}

}
