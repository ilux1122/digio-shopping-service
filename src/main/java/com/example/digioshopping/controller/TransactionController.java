package com.example.digioshopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Transaction;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.User;
import com.example.digioshopping.repository.TransactionRepository;
import com.example.digioshopping.repository.MerchantRepository;
import com.example.digioshopping.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/transaction")
public class TransactionController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	TransactionRepository transactionRepository;

	@GetMapping
	public List<Transaction> index() {
		return transactionRepository.findAll();
	}
	@GetMapping("/{id}")
	public List<Transaction> showusername(@PathVariable String user) {
		return transactionRepository.findById(Integer.parseInt(user));
	}

	@GetMapping("/customer/{id}")
	public List<Transaction> showcusid(@PathVariable String id) {
		return transactionRepository.findBycustomerid(Integer.parseInt(id));
	}
	@GetMapping("/order/{id}")
	public List<Transaction> showorder(@PathVariable String id) {
		List<Transaction> a = null;
		try {
			a = transactionRepository.findByOrderid(id);
			if(a == null) {
				return null;
			}else {
				return a;
			}
		}catch(Exception e) {
			return null;
		}
	}

	@PostMapping
	public Response createTransaction(@RequestBody Transaction user) {
		try {
			re = vd.SetTransaction(user);
			if (re.getCode() == "000") {
					Transaction u = transactionRepository.save(new Transaction(user.getCustomerid(), user.getTransactiontype(), user.getAmount(), user.getTransactiontime(),user.getOrderid()));
					return vd.SetTransactionsuccess(u);
				
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetTransactionsuccess(null);
		}
	} 

	@PutMapping("/{id}")
	public Response updateUser(@PathVariable String id, @RequestBody Transaction user) {
		try {
			re = vd.UpdateTransactionfromID(id, user);
			if (re.getCode() == "000") {
				Transaction u = transactionRepository.save(new Transaction(user.getTransactionid(), user.getCustomerid(), user.getTransactiontype(), user.getAmount(), user.getTransactiontime(),user.getOrderid()));
				return vd.UpdateTransactionfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateTransactionfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteTransaction(@PathVariable String id) {
		try {
			re = vd.DeleteTransaction(id);
			if (re.getCode() == "000") {
				try {
					transactionRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteTransactionsuccess(true);
				} catch (Exception e) {
					return vd.DeleteTransactionsuccess(false);
				}
			}
			return vd.DeleteTransactionsuccess(false);
		} catch (Exception e) {
			return vd.DeleteTransactionsuccess(false);
		}
	}

}
