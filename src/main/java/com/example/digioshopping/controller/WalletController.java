package com.example.digioshopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Wallet;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.User;
import com.example.digioshopping.repository.WalletRepository;
import com.example.digioshopping.repository.MerchantRepository;
import com.example.digioshopping.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/wallet")
public class WalletController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	WalletRepository walletRepository;

	@GetMapping
	public List<Wallet> index() {
		return walletRepository.findAll();
	}
	@GetMapping("/{user}")
	public Wallet showusername(@PathVariable String user) {
		return walletRepository.findById(Integer.parseInt(user));
	}

	@PostMapping
	public Response createWallet(@RequestBody String user) {
		try {
			re = vd.SetWallet(user);
			if (re.getCode() == "000") {
					Wallet u = walletRepository.save(new Wallet(0, 0.0, user));
					return vd.SetWalletsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetWalletsuccess(null);
		}
	}

	@PutMapping("/{id}")
	public Response updateUser(@PathVariable String id, @RequestBody Wallet user) {
		try {
			re = vd.UpdateWalletfromIDs(id, user);
			if (re.getCode() == "000") {
				Wallet u = walletRepository.save(new Wallet(user.getWalletid(), user.getMoney(), user.getSecretkey()));
				return vd.UpdateWalletfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateWalletfromIDsuccess(null);
		}
	}
	@PutMapping("/addamount/{id}")
	public Response addamount(@PathVariable String id, @RequestBody Wallet user) {
		System.out.println("add"+user.getMoney());
		try {
			re = vd.UpdateWalletfromIDs(id, user);
			if (re.getCode() == "000") {
				Wallet u = walletRepository.save(new Wallet(Integer.parseInt(id), walletRepository.findById(Integer.parseInt(id)).getMoney()+user.getMoney(), walletRepository.findById(Integer.parseInt(id)).getSecretkey()));

				System.out.println("added"+u.getWalletid()+" "+u.getMoney()+" ");
				return vd.UpdateWalletfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateWalletfromIDsuccess(null);
		}
	}

	@PutMapping("/minusamount/{id}")
	public Response minusamount(@PathVariable String id, @RequestBody Wallet user) {
		System.out.println("minus"+user.getMoney());
		try {
			re = vd.UpdateWalletfromIDs(id, user);
			if (re.getCode() == "000") {
				Wallet u = walletRepository.save(new Wallet(Integer.parseInt(id), walletRepository.findById(Integer.parseInt(id)).getMoney()-user.getMoney(), walletRepository.findById(Integer.parseInt(id)).getSecretkey()));
				return vd.UpdateWalletfromIDsuccess(u);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateWalletfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteWallet(@PathVariable String id) {
		try {
			re = vd.DeleteWallet(id);
			if (re.getCode() == "000") {
				try {
					walletRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteWalletsuccess(true);
				} catch (Exception e) {
					return vd.DeleteWalletsuccess(false);
				}
			}
			return vd.DeleteWalletsuccess(false);
		} catch (Exception e) {
			return vd.DeleteWalletsuccess(false);
		}
	}

}
