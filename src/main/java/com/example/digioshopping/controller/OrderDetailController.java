package com.example.digioshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Orderdetail;
import com.example.digioshopping.model.Orders;
import com.example.digioshopping.repository.OrderDetailRepository;
import com.example.digioshopping.repository.OrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/orderdetail")
public class OrderDetailController {

	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	OrderDetailRepository orderdetailRepository;

	@Autowired
	OrderRepository orderRepository;

	@GetMapping
	public List<Orderdetail> index() {

		return orderdetailRepository.findAll();
	}

	@GetMapping("/getfromorder/{id}")
	public List<Orderdetail> show(@PathVariable String id) {
		return orderdetailRepository.findByOrderid(Integer.parseInt(id));
	}

	@PostMapping
	public Response createMerchant(@RequestBody List<Orderdetail> or) {
		for (Orderdetail o : or) {
			try {
				Orderdetail od = new Orderdetail(o.getProductid(), o.getProductname(), o.getQuantity(), o.getPrice(),o.getOrderid());
				re = vd.SetOrderDetail(od);
if (re.getCode() == "000") {
					Orderdetail oo = orderdetailRepository.save(od);
					if (vd.SetOrderDetailsuccess(oo).getCode() != "000") {
						return re;
					}
				} else {
					return re;
				}
			} catch (Exception e) {
				return vd.SetOrderDetailsuccess(null);
			}
		}
		return re;
	}

	@PutMapping("/{id}")
	public Response updateMerchant(@PathVariable String id, @RequestBody Orderdetail or) {
		try {
			re = vd.UpdateOrderDetailfromID(id, or);
			if (re.getCode() == "000") {
				Orderdetail o = orderdetailRepository.save(new Orderdetail(Integer.parseInt(id), or.getProductid(),
						or.getProductname(), or.getQuantity(), or.getPrice(), or.getOrderid()));
				return vd.UpdateOrderDetailfromIDsuccess(o);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.UpdateOrderDetailfromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteMerchant(@PathVariable String id) {
		try {
			re = vd.DeleteOrderDetail(id);
			if (re.getCode() == "000") {
				try {
					orderdetailRepository.deleteById(Integer.parseInt(id));
					return vd.DeleteOrderDetailsuccess(true);
				} catch (Exception e) {
					return vd.DeleteOrderDetailsuccess(false);
				}
			}
			return vd.DeleteOrderDetailsuccess(false);
		} catch (Exception e) {
			return vd.DeleteOrderDetailsuccess(false);
		}
	}
}
