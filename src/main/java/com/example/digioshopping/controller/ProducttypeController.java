package com.example.digioshopping.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.digioshopping.Response;
import com.example.digioshopping.Validate;
import com.example.digioshopping.model.Producttype;
import com.example.digioshopping.repository.ProducttypeRepository;

@CrossOrigin
@RestController
@RequestMapping("/producttype")
public class ProducttypeController {
	Response re = new Response();
	Validate vd = new Validate();
	@Autowired
	ProducttypeRepository producttypeRespository;

	@GetMapping
	public List<Producttype> index() {
		return producttypeRespository.findAll();
	}

	@GetMapping("/{id}")
	public List<Producttype> show(@PathVariable String id) {
		int ptid = Integer.parseInt(id);
		return producttypeRespository.findById(ptid);
	}

	@PostMapping
	public Response createProducttype(@RequestBody Producttype pt) {
		try {
			re = vd.SetProducttype(pt);
			if (re.getCode() == "000") {
				Producttype p = producttypeRespository.save(new Producttype(pt.getProducttypeid(), pt.getTypename()));
				return vd.SetProducttypesuccess(p);
			} else {
				return re;
			}
		} catch (Exception e) {
			return vd.SetProducttypesuccess(null);
		}
	}

	@PutMapping("/{id}")
	public Response updateProducttype(@PathVariable String id, @RequestBody Producttype pt) {
		try {
		re = vd.UpdateProducttypefromID(id,pt);
		if (re.getCode() == "000") {
			Producttype p = producttypeRespository.save(new Producttype(Integer.parseInt(id), pt.getTypename()));
		return vd.UpdateProducttypefromIDsuccess(p);
		} else {
			return re;
		}}catch(Exception e) {
			return vd.UpdateProducttypefromIDsuccess(null);
		}
	}

	@DeleteMapping("/{id}")
	public Response deleteProducttype(@PathVariable String id) {
		try {
		re = vd.DeleteProducttype(id);
		if (re.getCode() == "000") {
			try {
		producttypeRespository.deleteById(Integer.parseInt(id));
		return vd.DeleteProducttypesuccess(true);
			} catch (Exception e) {
				return vd.DeleteProducttypesuccess(false);
			}
		}
		return vd.DeleteProducttypesuccess(false);
	}catch(Exception e) {
		return vd.DeleteProducttypesuccess(false);
	}
	}
	
}
