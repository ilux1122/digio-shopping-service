package com.example.digioshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.digioshopping.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

    // custom query to search to blog post by title or content
    List<User> findAll();
    User findByUsername(String username);
}
