package com.example.digioshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.digioshopping.model.Orderdetail;
import com.example.digioshopping.model.Orders;

public interface OrderDetailRepository extends JpaRepository<Orderdetail, Integer> {

    List<Orderdetail> findAll();

	List<Orderdetail> findByOrderid(int id);
	

}
