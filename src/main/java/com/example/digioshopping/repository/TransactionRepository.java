package com.example.digioshopping.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.digioshopping.model.Customer;
import com.example.digioshopping.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    
	List<Transaction> findAll();
	List<Transaction> findByOrderid(String id);
    List<Transaction> findById(int id);
	List<Transaction> findBycustomerid(int parseInt);
}
