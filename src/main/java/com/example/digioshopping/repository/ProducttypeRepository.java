package com.example.digioshopping.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.digioshopping.model.Producttype;
import com.example.digioshopping.model.Wallet;
public interface ProducttypeRepository extends JpaRepository<Producttype, Integer> {

    // custom query to search to blog post by title or content
    List<Producttype> findAll();
    List<Producttype> findById(int id);

}
