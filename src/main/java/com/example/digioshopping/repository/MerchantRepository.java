package com.example.digioshopping.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.digioshopping.model.Merchant;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Integer> {
    List<Merchant> findAll();
	Merchant findByUsername(String username);
}
