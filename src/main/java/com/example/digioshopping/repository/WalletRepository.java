package com.example.digioshopping.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.digioshopping.model.Customer;
import com.example.digioshopping.model.Wallet;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Integer> {
    List<Wallet> findAll();
    Wallet findById(int id);
}
