package com.example.digioshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.digioshopping.model.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

    List<Orders> findAll();

	List<Orders> findByMerchantid(int id);
}
