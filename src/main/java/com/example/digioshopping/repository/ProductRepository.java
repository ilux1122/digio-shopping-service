package com.example.digioshopping.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.digioshopping.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findAll();
    
    Product save(Product p);

	List<Product> findBymerchantid(int ptid);

	Product findByproductid(int ptid);

}
