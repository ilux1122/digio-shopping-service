package com.example.digioshopping.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.digioshopping.model.Customer;
import com.example.digioshopping.model.Merchant;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    List<Customer> findAll();
    Customer findByUsername(String username);
    List<Customer> findById(int id);
	Customer findByTel(String tel);
}
