package com.example.digioshopping;

import java.util.List;

import com.example.digioshopping.model.Customer;
import com.example.digioshopping.model.Merchant;
import com.example.digioshopping.model.Orderdetail;
import com.example.digioshopping.model.Orders;
import com.example.digioshopping.model.Product;
import com.example.digioshopping.model.Producttype;
import com.example.digioshopping.model.Transaction;
import com.example.digioshopping.model.User;
import com.example.digioshopping.model.Wallet;

public class Validate {

	public Response SetMerchant(Merchant in) {
		if (in.getMerchantname() == null || in.getMerchantname() == "") {
			return new Response("505", "Merchantname is Empty!Please try again.");
		}
		if (in.getUsername() == null || in.getUsername() == "") {
			return new Response("505", "Username is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving Merchant!");
		}
		return new Response("000", "successfully");

	}
	public Response SetMerchantsuccess(Merchant out) {
		if (out.getMerchantname() != null && out.getUsername() != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "create Merchant fail!");

	}
	public Response UpdateMerchantfromID(String id, Merchant in) {
		if (id == null || id == "") {
			return new Response("505", "MerchantID is Empty!Please try again.");
		}
		if (in.getMerchantname() == null || in.getMerchantname() == "") {
			return new Response("505", "Merchantname is Empty!Please try again.");
		}
		if (in.getUsername() == null || in.getUsername() == "") {
			return new Response("505", "Username is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving Merchant!");
		}
		return new Response("000", "successfully");

	}
	public Response UpdateMerchantfromIDsuccess(Merchant out) {
		if (out.getMerchantname() != null && out.getUsername() != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "Error Merchant fail!");

	}
	public Response DeleteMerchant(String id) {
		if (id != null && id != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "MerchantID is Empty!Please try again.");
		}
	}
	public Response DeleteMerchantsuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Merchant fail!");
		}
	}
	
	public Response SetOrder(Orders in) {
		if (in.getCustomername() == null || in.getCustomername() == "") {
			System.out.println("Customername");
			return new Response("505", "Customername is Empty!Please try again.");
		}
		if (in.getMerchantid() + "" == null || in.getMerchantid() + "" == "") {
			System.out.println("Merchantid");
			return new Response("505", "Merchantid is Empty!Please try again.");
		}
		if (in.getTotalprice() == null || in.getTotalprice() + "" == "" || in.getTotalprice() == 0) {
			System.out.println("Totalprice");
			return new Response("505", "Totalprice is Empty!Please try again.");
		}
		if (in.getTransactiontime() == null || in.getTransactiontime() == "") {
			System.out.println("Transactiontime");
			return new Response("505", "Transactiontime is Empty!Please try again.");
		}if(in == null) {
			System.out.println("404");
			return new Response("404", "Error receiving Orders!");
		}

		System.out.println("000");
		return new Response("000", "successfully");
	}
	public Response SetOrdersuccess(Orders out) {
		if (out != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "create Orders fail!");
	}
	public Response UpdateOrderfromID(String id, Orders in) {
		if (id == null || id == "") {
			return new Response("505", "MerchantID is Empty!Please try again.");
		}
		if (in.getCustomername() == null || in.getCustomername() == "") {
			return new Response("505", "Customername is Empty!Please try again.");
		}
		if (in.getMerchantid() + "" == null || in.getMerchantid() + "" == "") {
			return new Response("505", "Merchantid is Empty!Please try again.");
		}
		if (in.getTotalprice() == null || in.getTotalprice() + "" == "" || in.getTotalprice() == 0) {
			return new Response("505", "Totalprice is Empty!Please try again.");
		}
		if (in.getTransactiontime() == null || in.getTransactiontime() == "") {
			return new Response("505", "Transactiontime is Empty!Please try again.");
		}
		if(in == null) {
		return new Response("404", "Error receiving Orders!");
		}
		return new Response("000", "successfully");
	}
	public Response UpdateOrderfromIDsuccess(Orders out) {
		if (out != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "Update Order fail!");

	}
	public Response DeleteOrder(String id) {
		if (id != null && id != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "OrderID is Empty!Please try again.");
		}
	}
	public Response DeleteOrdersuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Order fail!");
		}
	}

	public Response SetOrderDetail(Orderdetail in) {
			
		
		if (in.getOrderid() + "" == null || in.getOrderid() + "" == "") {
			return new Response("505", "OrderDetailID is Empty!Please try again.");
		}
		if (in.getPrice() == null || in.getPrice().toString() == "" || in.getPrice() == 0) {
			return new Response("505", "OrderDetailPrice is Empty!Please try again.");
		}
		if (in.getProductname() == null || in.getProductname() + "" == "") {
			return new Response("505", "ProductName is Empty!Please try again.");
		}
		if (in.getQuantity() + "" == null || in.getQuantity() + "" == "" || in.getQuantity() == 0) {
			return new Response("505", "Quantity is Empty!Please try again.");
		}
		if(in == null) {
		return new Response("404", "Error receiving OrderDetail!");
		}
		return new Response("000", "successfully");

	}
	public Response SetOrderDetailsuccess(Orderdetail out) {
		if (out != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "create OrderDetail fail!");

	}
	public Response UpdateOrderDetailfromID(String id, Orderdetail in) {
		if (id == null || id == "") {
			return new Response("505", "MerchantID is Empty!Please try again.");
		}
		if (in.getOrderid() + "" == null || in.getOrderid() + "" == "") {
			return new Response("505", "OrderDetailID is Empty!Please try again.");
		}
		if (in.getPrice() == null || in.getPrice() + "" == "" || in.getPrice() == 0) {
			return new Response("505", "OrderDetailPrice is Empty!Please try again.");
		}
		if (in.getProductname() == null || in.getProductname() + "" == "") {
			return new Response("505", "ProductName is Empty!Please try again.");
		}
		if (in.getQuantity() + "" == null || in.getQuantity() + "" == "" || in.getQuantity() == 0) {
			return new Response("505", "Quantity is Empty!Please try again.");
		}

		if(in == null) {
		return new Response("404", "Error receiving OrderDetail!");
		}
		return new Response("000", "successfully");
	}
	public Response UpdateOrderDetailList(List<Orderdetail> inner) {
		for(Orderdetail in : inner) {
		if (in.getOrderid() + "" == null || in.getOrderid() + "" == "") {
			return new Response("505", "OrderDetailID is Empty!Please try again.");
		}
		if (in.getPrice() == null || in.getPrice() + "" == "" || in.getPrice() == 0) {
			return new Response("505", "OrderDetailPrice is Empty!Please try again.");
		}
		if (in.getProductname() == null || in.getProductname() + "" == "") {
			return new Response("505", "ProductName is Empty!Please try again.");
		}
		if (in.getQuantity() + "" == null || in.getQuantity() + "" == "" || in.getQuantity() == 0) {
			return new Response("505", "Quantity is Empty!Please try again.");
		}

		if(in == null) {
		return new Response("404", "Error receiving OrderDetail!");
		}
		}
		return new Response("000", "successfully");
	}
	public Response UpdateOrderDetailfromIDsuccess(Orderdetail out) {
		if (out != null) {
			return new Response("000", "successfully");
		}
		return new Response("404", "Update OrderDetail fail!");

	}
	public Response DeleteOrderDetail(String id) {
		if (id != null && id != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "OrderDetailID is Empty!Please try again.");
		}
	}
	public Response DeleteOrderDetailsuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete OrderDetail fail!");
		}
	}
	
	public Response SetProduct(Product in) {
		
		if (in.getMerchantid() + "" == null || in.getMerchantid() + "" == "") {
			return new Response("505", "Merchantid is Empty!Please try again.");
		}
		if (in.getProductimg() == null || in.getProductimg().toString() == "") {
			return new Response("505", "Image Product is Empty!Please try again.");
		}
		if (in.getProductname() == null || in.getProductname() + "" == "") {
			return new Response("505", "Productname is Empty!Please try again.");
		}
		if (in.getProductprice() + "" == null || in.getProductprice() + "" == "" || in.getProductprice() == 0) {
			return new Response("505", "Productprice is Empty!Please try again.");
		}
		if (in.getProducttypeid() + "" == null || in.getProducttypeid() + "" == "" || in.getProducttypeid() == 0) {
			return new Response("505", "Producttype is Empty!Please try again.");
		}
		if (in.getQuantity() + "" == null || in.getQuantity() + "" == "" || in.getQuantity() == 0) {
			return new Response("505", "Quantity is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving Product!");
			}
			return new Response("000", "successfully");

	}
	public Response SetProductsuccess(Product out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create Product fail!");
		}
	}
	public Response UpdateProductfromID(String id, Product in) {
		if (id == null || id == "") {
			return new Response("505", "Productid is Empty!Please try again.");
		}
		if (in.getMerchantid() + "" == null || in.getMerchantid() + "" == "") {
			return new Response("505", "Merchantid is Empty!Please try again.");
		}
		if (in.getProductimg() == null || in.getProductimg().toString() == "") {
			return new Response("505", "Image Product is Empty!Please try again.");
		}
		if (in.getProductname() == null || in.getProductname() + "" == "") {
			return new Response("505", "Productname is Empty!Please try again.");
		}
		if (in.getProductprice() + "" == null || in.getProductprice() + "" == "") {
			return new Response("505", "Productprice is Empty!Please try again.");
		}
		if (in.getProducttypeid() + "" == null || in.getProducttypeid() + "" == "") {
			return new Response("505", "Producttype is Empty!Please try again.");
		}
		if (in.getQuantity() + "" == null || in.getQuantity() + "" == "") {
			return new Response("505", "Quantity is Empty!Please try again.");
		}if(in == null) {
			return new Response("404", "Error receiving Product!");
			}
			return new Response("000", "successfully");
	}
	public Response UpdateProductfromIDsuccess(Product out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update Product fail!");
		}

	}
	public Response DeleteProduct(String id) {
		if (id != null && id != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "ProductID is Empty!Please try again.");
		}
	}
	public Response DeleteProductsuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Product fail!");
		}
	}
	   
	public Response SetProducttype(Producttype in) {
		if (in.getTypename() == null || in.getTypename() == "") {
			return new Response("505", "Typename is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving Producttype!");
			}
			return new Response("000", "successfully");
	}
	public Response SetProducttypesuccess(Producttype out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create Producttype fail!");
		}
	}
	public Response UpdateProducttypefromID(String id, Producttype in) {
		if(id == null || id == "") {
			return new Response("505", "ProducttypeID is Empty!Please try again.");
		}
		if (in.getTypename() == null || in.getTypename() == "") {
			return new Response("505", "Typename is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving Producttype!");
			}
			return new Response("000", "successfully");
	}
	public Response UpdateProducttypefromIDsuccess(Producttype out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update Producttype fail!");
		}

	}
	public Response DeleteProducttype(String id) {
		if (id != null && id != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "ProducttypeID is Empty!Please try again.");
		}
	}
	public Response DeleteProducttypesuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Producttype fail!");
		}
	}
    
	public Response SetUser(User in) {
		if (in.getUsername() == null || in.getUsername() == "") {
			return new Response("505", "Username is Empty!Please try again.");
		}
		if (in.getPassword() == null || in.getPassword() == "") {
			return new Response("505", "Password is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response SetUsersuccess(User out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create User fail!");
		}
	}
	public Response UpdateUserfromID(String user, User in) {
		if(user == null || user == "") {
			return new Response("505", "Username is Empty!Please try again.");
			}
		if (in.getUsername() == null || in.getUsername() == "") {
			return new Response("505", "Username is Empty!Please try again.");
		}
		if (in.getPassword() == null || in.getPassword() == "") {
			return new Response("505", "Password is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response UpdateUserfromIDsuccess(User out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update User fail!");
		}

	}
	public Response DeleteUser(String user) {
		if (user != null && user != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "Username is Empty!Please try again.");
		}
	}
	public Response DeleteUsersuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Username fail!");
		}
	}

	public Response SetCustomer(Customer in) {
		if (in.getUsername() == null || in.getUsername() == "") {
			return new Response("505", "Username is Empty!Please try again.");
		}
		if (in.getBirthday() == null || in.getBirthday() == "") {
			return new Response("505", "Birthday is Empty!Please try again.");
		}
		if (in.getFirstname() == null || in.getFirstname() == "") {
			return new Response("505", "Firstname is Empty!Please try again.");
		}
		if (in.getLastname() == null || in.getLastname() == "") {
			return new Response("505", "Lastname is Empty!Please try again.");
		}

		if (in.getTel() == null || in.getTel() == "") {
			return new Response("505", "Tel is Empty!Please try again.");
		}
		if (in.getWalletid() == 0 || in.getWalletid()+"" == "") {
			return new Response("505", "Wallet is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response SetCustomersuccess(Customer out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create User fail!");
		}
	}
	public Response UpdateCustomerfromID(String user, Customer in) {
			if (in.getUsername() == null || in.getUsername() == "") {
				return new Response("505", "Username is Empty!Please try again.");
			}
			if (in.getBirthday() == null || in.getBirthday() == "") {
				return new Response("505", "Birthday is Empty!Please try again.");
			}
			if (in.getFirstname() == null || in.getFirstname() == "") {
				return new Response("505", "Firstname is Empty!Please try again.");
			}
			if (in.getLastname() == null || in.getLastname() == "") {
				return new Response("505", "Lastname is Empty!Please try again.");
			}

			if (in.getTel() == null || in.getTel() == "") {
				return new Response("505", "Tel is Empty!Please try again.");
			}
			if (in.getWalletid() == 0 || in.getWalletid()+"" == "") {
				return new Response("505", "Wallet is Empty!Please try again.");
			}
			if(in == null) {
				return new Response("404", "Error receiving User!");
				}
				return new Response("000", "successfully");
				
	}
	public Response UpdateCustomerfromIDsuccess(Customer out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update User fail!");
		}

	}
	public Response DeleteCustomer(String user) {
		if (user != null && user != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "Username is Empty!Please try again.");
		}
	}
	public Response DeleteCustomersuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Username fail!");
		}
	}

	public Response SetWallet(String in) {
		if (in == null || in == "") {
			return new Response("505", "Secretkey is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response SetWalletsuccess(Wallet out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create User fail!");
		}
	}
	public Response UpdateWalletfromIDs(String user, Wallet in) {
		if (in.getMoney()+"" == "" ) {
			return new Response("505", "Money is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");	
	}
	public Response UpdateWalletfromID(String user, String in) {
		if (in+"" == "" || in == null) {
			return new Response("505", "Money is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");	
	}
	public Response UpdateWalletfromIDsuccess(Wallet out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update User fail!");
		}

	}
	public Response DeleteWallet(String user) {
		if (user != null && user != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "Username is Empty!Please try again.");
		}
	}
	public Response DeleteWalletsuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Username fail!");
		}
	}

	public Response SetTransaction(Transaction in) {
		if (in.getAmount()+"" == "") {
			return new Response("505", "Amount is Empty!Please try again.");
		}
		if (in.getCustomerid()+"" == "") {
			return new Response("505", "Customer is Empty!Please try again.");
		}
		if (in.getTransactiontime() == null || in.getTransactiontime() == "") {
			return new Response("505", "Transactiontime is Empty!Please try again.");
		}
		if (in.getTransactiontype() == null || in.getTransactiontype() == "") {
			return new Response("505", "Transactiontype is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response SetTransactionsuccess(Transaction out) {
		if (out != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "create User fail!");
		}
	}
	public Response UpdateTransactionfromID(String user, Transaction in) {
		if (in.getAmount()+"" == "") {
			return new Response("505", "Amount is Empty!Please try again.");
		}
		if (in.getCustomerid()+"" == "") {
			return new Response("505", "Customer is Empty!Please try again.");
		}
		if (in.getTransactiontime() == null || in.getTransactiontime() == "") {
			return new Response("505", "Transactiontime is Empty!Please try again.");
		}
		if (in.getTransactiontype() == null || in.getTransactiontype() == "") {
			return new Response("505", "Transactiontype is Empty!Please try again.");
		}
		if(in == null) {
			return new Response("404", "Error receiving User!");
			}
			return new Response("000", "successfully");
	}
	public Response UpdateTransactionfromIDsuccess(Transaction u) {
		if (u != null) {
			return new Response("000", "successfully");
		}else {
		return new Response("404", "Update User fail!");
		}

	}
	public Response DeleteTransaction(String user) {
		if (user != null && user != "") {
			return new Response("000", "successfully");
		} else {
			return new Response("505", "Username is Empty!Please try again.");
		}
	}
	public Response DeleteTransactionsuccess(boolean alert) {
		if (alert == true) {
			return new Response("000", "successfully");
		} else {
			return new Response("404", "Delete Username fail!");
		}
	}
	
}
